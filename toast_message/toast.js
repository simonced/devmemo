
/**
 * Display a toast message at the top of the screen
 * @param {string} msg_ Message to show (in HTML)
 * @param {string} class_ class to apply to the message, defaults are:
 *  - error (red)
 *  - warning (orange)
 *  - info (blue)
 *  - success (green)
 */
function showToast(msg_, class_="error")
{
	const showTimeout = 100;
	const autoHideTimeout = 3000;

	let toast = document.createElement("div");
	toast.classList.add("toast", class_);
	toast.innerHTML = msg_;

	// dismiss toast by clicking on it
	toast.addEventListener("click", function() {
		this.classList.add("delete");
	});
	toast.addEventListener("transitionend", function(e_) {
		if(this.classList.contains("delete")) this.remove();
	});

	// auto hide
	setTimeout(x => {
		toast.classList.add("delete");
	}, autoHideTimeout);

	// show Toast
	window.document.body.append(toast);
	setTimeout(function() {
		toast.classList.add("bounceIn");
	}, showTimeout);
}