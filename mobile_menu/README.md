How to use
==========

Simply include `mobile_menu.js` and `mobile_menu.css` in the page.

The only things to be careful of, are the css class names (see below).

The main idea is to let the mobile menu be displayed with JS.  
The "Desktop" parts are hidden with CSS.  
The first time the mobile menu is opened (tapped), the content of 2 areas are "copied" into the mobile menu.  
For now, `#main-menu-list` and `#side-menu-content`.  
You can customize to add or remove content at your liking.

Dependencies
------------

- jQuery


Sample HTML
-----------

See the [sample.html](sample.html) file.

The `mobile_menu.css` only defines the bare minimum regarding the mobile-menu.

All other styles can stay in your site's main CSS files as usual.


CSS classes meaning
====================

.mobile-menu-only
-----------------

Element is only displayable on mobile (see break-point in CSS, customize if needed).


.mobile-menu-open-btn
---------------------

Element that will open the mobile menu.  
To be used together with `.mobile-menu-only`.

