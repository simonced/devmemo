var MobileMenu = {
	/** @type {HTMLElement} */
	button: null,

	/**
	 * init mobile menu
	 * @param {string} button_
	 * @returns void
	 */
	init: function(button_) {
		// buttons click events
		document.querySelectorAll(button_).forEach(x => {
			x.addEventListener("click", this.toggleMenu);
		});
	},

	/**
	 * Toggle mobile menu
	 * @param {Event} event_ 
	 */
	toggleMenu: function(event_) {
		// console.log( event_ ); // DBG
		event_.preventDefault();
		document.body.classList.toggle("mobile-menu-open");
	},
}