<?php

class CsvHelper
{

	private $headers = []; // eg,： ['year' => 'Year', ...]
	private $maps    = []; // callbacks for columns data (optional)


	/**
	 * Utility: remove line breaks
	 *
	 * @param string $txt_
	 * @return string
	 */
	static function noBreakLine($txt_)
	{
		$oneline = nl2br($txt_);

		$oneline = preg_replace("!<br *?\/?>!", "", $oneline);

		return $oneline;
	}


	/**
	 * CSV file name for download
	 *
	 * @param string $file_name_
	 * @return void
	 */
	static function httpHeaders($file_name_='download.csv')
	{
		header("Content-type: application/csv;");
		header("Content-Disposition: attachment; filename=$file_name_");
	}


	/**
	 * Change content encoding from utf-8 into sjis
	 * （support of MsExcel）
	 *
	 * @param string $content_
	 * @return string
	 */
	static function convertUft8ToSjis($content_)
	{
		return mb_convert_encoding($content_, "SJIS", "UTF-8");
	}


	/**
	 * Change content encoding from sjis into utf-8
	 *
	 * @param string $content_
	 * @return string
	 */
	static function convertSjisToUft8($content_)
	{
		return mb_convert_encoding($content_, "UTF-8", "SJIS");
	}


	/**
	 * Convert all dates into ISO
	 *
	 * @param array[] $lines_
	 * @param array $columns_ name of the columns we want to format
	 * @return array[]
	 */
	static function formatDateIsoAll($lines_, $columns_)
	{
		$fixed_lines = []; // return

		foreach($lines_ as $line) {
			$temp = []; // new line

			foreach($line as $k => $v) {
				if(in_array($k, $columns_)) $temp[$k] = self::formatDateIso($v);
				else $temp[$k] = $v; // no formating
			}

			// add to result
			$fixed_lines[] = $temp;
		}

		return $fixed_lines;
	}


	/**
	 * Utility: Format date into ISO
	 * (useful for excel which format isYYYY-MM-DD)
	 *
	 * @param string $date_ (YYYY/MM/DD or YYYY-MM-DD already formated date can be passed too!)
	 * @return string
	 */
	static function formatDateIso($date_)
	{
		$parts = preg_split("![/-]!", $date_);

		return sprintf("%s-%02d-%02d", $parts[0], $parts[1], $parts[2]);
	}


	/**
	 * Shows lines of data in an HTML talbe
	 * (for debugging)
	 *
	 * @param array[] $lines_
	 * @return string
	 */
	function debugTable($lines_)
	{
		$html = "<table>";

		// headers
		$html .= "<thead><tr>";
		foreach($this->headers as $header) {
			$html .= "<th>$header</th>";
		}
		$html .= "</tr></thead>";

		// data
		$html .= "<tbody>";
		foreach($lines_ as $line) {
			$html .= "<tr>";
			foreach($this->headers as $k => $column) {
				if($this->maps[$k]) {
					// use modifier
					$temp = $this->maps[$k]($line);
					$html .= "<td>$temp</td>";
				}
				else {
					$html .= "<td>{$line[$k]}</td>";
				}
			}
			$html .= "</tr>";
		}
		$html .= "</tbody>";

		// close table
		$html .= "</table>";

		return $html;
	}


	function __construct()
	{
	}


	/**
	 * Register CSV headers column names
	 * (key => name pairs)
	 *
	 * @param array $config_
	 * @return void
	 */
	function setHeaders($config_)
	{
		$this->headers = $config_; // data-> columns config (for display)
	}


	/**
	 * Register maps of callbacks to convert data from source to CSV
	 *
	 * @param callable[] $maps_ [header_key => callable]
	 * Callable takes one argument, it's the full line.
	 * Callables are optional, if none is registered for a column, it'll be returned untouched.
	 * @return void
	 */
	function setMaps($maps_)
	{
		$this->maps = $maps_; // callback per column (if needed)
	}


	/**
	 * Apply the column mappings.
	 * (To be called before sending the CSV)
	 *
	 * @param array[] $data_
	 * @return array[]
	 */
	function mapData($data_)
	{
		$mapped = []; // return

		foreach($data_ as $line) {
			// for each data line
			$temp = [];
			foreach(array_keys($this->headers) as $column) {
				// check each column (key, not display name)
				try {
					// apply mapping if any
					if($this->maps[$column]) {
						// column entry data from the current line
						$temp[$column] = $this->maps[$column]($line);
					}
					else if(is_array($line)) {
						$entry = $line[$column];
						$temp[$column] = $entry;
					}
					else {
						throw new Exception("no handling for data");
					}
				}
				catch(Exception $e_) {
					$temp[$column] = "error";
				}
			}
			$mapped[] = $temp;
		}

		return $mapped;
	}


	/**
	 * Tunrs array of data into CSV text
	 *
	 * @param array[] $lines_
	 * @return string (csv content)
	 */
	function makeCSV($lines_)
	{
		$csv = "";
		// headers
		$row = [];
		foreach($this->headers as $header) {
			$row[] = "\"$header\"";
		}
		$csv .= implode(',', $row)."\n";
		// data
		foreach($lines_ as $line) {
			$row = [];
			foreach($line as $column) {
				$row[] = "\"$column\"";
			}
			$csv .= implode(',', $row)."\n";
		}
		return $csv;
	}


	/**
	 * Read CSV file
	 *
	 * @param string $file_ (csv file name)
	 * @param array $columns_
	 * @return array[]
	 */
	function readCSV($file_, $columns_)
	{
		$lines = []; // return

		// columns positions
		$columns_positions = $this->columnsPositions($columns_);
		// var_dump( $columns_positions ); // DBG
		// read file
		$file = new SplFileObject($file_);
		// set all flags to clear empty lines
		$file->setFlags(SplFileObject::READ_CSV | SplFileObject::READ_AHEAD | SplFileObject::SKIP_EMPTY | SplFileObject::DROP_NEW_LINE);
		
		// read rows
		foreach($file as $row) {
			$lines[] = $this->csvColumns($row, $columns_positions);
		}
	
		return $lines;
	}


	/**
	 * Calculate the CSV column headers position (for import)
	 *
	 * @param array $columns_ (flat list of columns)
	 * @return array ['name' => 1, 'address' => 2, ...]
	 */
	private function columnsPositions($columns_)
	{
		$positions = []; // return

		// column indexes
		$headers_indexes = array_flip(array_keys($this->headers));
		/*
			eg,：
			[
				'name' => 'YYY',
				'address' => 'XXX',
			]
			becomes:
			[
				'name' => 0,
				'address' => 1,
			]
		*/
		
		foreach($columns_ as $c) {
			if(key_exists($c, $headers_indexes) === false) continue;

			$positions[$c] = $headers_indexes[$c];
		}

		return $positions;
	}


	/**
	 * Return CSV line with column names.
	 * (When reading the CSV, this allows to map raw column data to DB column names)
	 *
	 * @param array $row_ (CSV line, each column is a cell of that array)
	 *  eg, [0 => 'col1', 1 => 'col2', ...]
	 * @param array $columns_positions_ 
	 *  eg, ['name' => 0, 'address' => 1, ...]
	 * @return array
	 *  eg, ['name' => 'col1', 'address' => 'col2', ...]
	 */
	private function csvColumns($row_, $columns_positions_)
	{
		$columns = []; // return
		$columns_names = array_flip($columns_positions_); // [ 7 => 'unit_key' ...]

		foreach($row_ as $i => $column)
		{
			if(in_array($i, $columns_positions_) === false) continue;

			$column_name = $columns_names[$i];
			$columns[$column_name] = self::convertSjisToUft8($column);
		};

		return $columns;
	}
}
