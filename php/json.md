# Important for old PHP (<5.2)

Here are replacement functions to use in place of `json_encode` and `json_decode`.  
https://github.com/alexmuz/php-json

I have other polyfill functions as well in the `polyfill.php` file.

# Server basics

Basic functions to get started.  

```php
/**
 * Send Json Answer, also sets headers.
 * 
 * @param  string $json_data_ already json encoded
 * @return void
 */
function sendJsonAnswer($json_data_)
{
	header("Content-Type: application/json");
	die($json_data_);
}


/**
 * making a JSON answer
 *
 * @param mixed $data_
 * @param bool $success_ flag
 * @param string $message_ error message if error
 * @return string JSON
 *  - success,
 *  - data,
 *  - message
 */
function makeJsonAnswer($data_, $success_=true, $message_='')
{
	echo json_encode([
		'success' => $success_,
		'message' => $message_,
		'data'    => $data_
	]);
}
```


## Usage sample 

In case of success:

```php
$data = getMyData(); // whatever function call generating data to send back
sendJsonAnswer(
	makeJsonAnswer($data)
);
```

In case of error:

```php
sendJsonAnswer(null, false, "Something went wrong!");
```


# Client helper function

That can serve as a base. It sends POST requests.

```php
/**
 * Send a request and get some JSON back
 *
 * @param array $data_
 * @return array answer from the API
 *  - success,
 *  - data,
 *  - message
 */
function sendRequestToAPI($url_, $data_)
{
	// use key 'http' even if you send the request to https://...
	$options = [
		'http' => [
			'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
			'method'  => 'POST',
			'content' => http_build_query($data_)
		]
	];
	$context  = stream_context_create($options);
	$answer = file_get_contents($url_, false, $context);

	return json_decode($answer, true);
}
```

