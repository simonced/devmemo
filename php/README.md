random piece of info found when working shared here:

# Older PHP version (<5.4)

Some functions were missing back in the days...  
I have a `polyfill.php` file that tries to fill the gaps.  
Gathered from github, I'll also add some mysql related stuff I did a long time ago when I have some time.

# post data with `file_get_contents`

[stackoverflow reference](http://stackoverflow.com/a/2445332/921796)

# CSV import Japanese with line-breaks!
Quite complexe procedure, but the solution is easy once you know!

```php
$lines = []; // resulting csv content

// backup current locale
$localeBkp = setlocale(LC_ALL, "0");
// set locale to CSV format
setlocale(LC_ALL, 'ja_JP.sjis');

$fp = fopen($file_, 'r');
while ($line = fgetcsv($fp)) {
    // convert the csv line entries into the UTF-8 encoding
    mb_convert_variables('UTF-8', 'sjis-win', $line);
    $lines[] = $line;
}
fclose($fp);

// restore the original locale
setlocale(LC_ALL, $localeBkp);

// you can use $lines the way you see fit :)
```

# CSV ouptput Japanese (shift-jis encoding)
In the sample below, `$results_` is an array formated like so:

    [0] => [
        'col1' => 'data 1',
        'col2' => 'data 2',
        ...
    ],
    ...

Sample PHP code:

```php
$fd = fopen('my_output.csv', "w");

// headers setup
$csv_cols = array(
    'col1'  => 'First Column Title',
    'col2'  => 'Second column Title',
);

// helper function
$make_csv_line = function($cols_) use($csv_cols) {
    $line = array();
    foreach(array_keys($csv_cols) as $col_name) {
        // data enclosed in "
        $line[] = '"'.str_replace('"', '""', $cols_[$col_name]).'"';
    }
    $csv_line = join(',', $line)."\n";

    $data = mb_convert_encoding($csv_line, "sjis-win", "UTF-8");
    return $data;
};

// headers
$data = $make_csv_line($csv_cols);
fwrite($fd, $data);

// lines
foreach($results_ as $row) {
    $data = $make_csv_line($row);
    fwrite($fd, $data);
}

fclose($fd);
```

# Generating a simple DB ER map

The class [dber.class.php](./dber.class.php) can help generating such file.  
The output will be in PNG.  
The class relies on [Graphviz](https://graphviz.org/) using the `neato` algorithm.

Sample usage:

```php
require_once "dber.class.php";

// output settings
define('OUTPUT_FILE', 'temp\mydb_er');
// ER instance
$er = new DBER('localhost', 'USERXXX', 'PASSXXX', 'DBNAMEXXX'); // adapt to your settings
// let's go!
$mapfile = $er->generateMap(OUTPUT_FILE);
```

`temp` folder should contain the temporary text file for graphviz, and the resulting png output file.


# Checking a value contains Katakana

The sample below checks for `katakana` and spaces only in a variable.

```php
$val = "..."; // testing value here
if(!preg_match("/^[ 　\p{Katakana}]+$/u", $val)) {
	// non valid entry...
}
```

# Clear empty lines in text

```php
preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $string);
```

# WordPress helpers

Some filters I use sometimes that can come in handy.

**File:** [wp.php](wp.php)
