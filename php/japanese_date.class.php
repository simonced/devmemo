<?php

class JapaneseDate
{
	// 基本設定
	static $gengo = array(
		'明治' => array(
			'from' => 1868,
			'to' => 1912,
		),
		'大正' => array(
			'from' => 1912,
			'to' => 1926,
		),
		'昭和' => array(
			'from' => 1926,
			'to' => 1989,
		),
		'平成' => array(
			'from' => 1989,
			'to' => 2019,
		),
		'令和' => array(
			'from' => 2019,
		),
	);


	/**
	 * 例：　昭和56　⇒　1981
	 *
	 * @param string $gengo_ 例：昭和
	 * @param int $nen_ 例：56
	 * @return int エラーの場合は、０になります
	 */
	static function JapaneseToGregorian($gengo_, $nen_) {
		$year = 0;

		if(isset(self::$gengo[$gengo_])) {
			list($from, $to) = array_values(self::$gengo[$gengo_]);
			$year =  $from + $nen_ - 1;
		}

		if($year > $to) {
			$year = 0; // input error!
		}

		return $year;
	}


	/**
	 * 例：　1981　⇒　昭和56
	 *
	 * @param int $year_ 例： 1981
	 * @return array(string, int) 例： array('昭和', 56)
	 */
	static function GregorianToJapanese($year_)
	{
		$gengo = '';
		$nen = 0;

		foreach(self::$gengo as $g => $y) {
			if(!isset($y['to'])) {
				$gengo = $g;
				$nen = $year_ - $y['from'] + 1;
				break;
			}
			elseif( $y['to']>= $year_ && $y['from']<=$year_ ) {
				$gengo = $g;
				$nen = $year_ - $y['from'] + 1;
				break;
			}
		}

		return array($gengo, $nen);
	}
}