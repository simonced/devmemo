<?php

class Validator
{

	/**
	 * check a text contains only ASCII characters
	 *
	 * @param string $val
	 * @return boolean
	 */
	static function isEnglishOnly($val)
	{
		$val_alpha = mb_convert_kana($val, 'as');
		return preg_match("/^[ -~]+$/", $val_alpha)===1;
	}


	/**
	 * check a text contains only Katakana
	 *
	 * @param string $val
	 * @return boolean
	 */
	static function isKatakanaOnly($val)
	{
		return preg_match("/^[\p{Katakana}]+$/u", $val)===1;
	}


	/**
	 * Count words in an english text.
	 *
	 * @param string $val
	 * @return int
	 */
	static function countEnglishWords($val)
	{
		$list = preg_split("/[ \n]+/i", $val);
		return count($list);
	}
}
