<?php


/**
 * Makes a mail header for utf-8 mail content
 *
 * @return string
 */
function mail_utf8_headers()
{
	$headers_common  = "MIME-Version: 1.0\r\n";
	$headers_common .= "Content-type: text/plain; charset=UTF-8\r\n";

	return $headers_common;
}


/**
 * Makes a mail subject encoded into UTF-8
 *
 * @param string $subject_
 * @return string
 */
function mail_utf8_subject($subject_)
{
	return '=?UTF-8?B?'.base64_encode($subject_).'?=';
}
