# Reference
Ref:
- https://web.dev/articles/custom-metrics?utm_source=devtools#server-timing-api
- https://w3c.github.io/server-timing/

# Server side
Simple script, that allows to see server side timings.
With PHP, we can simply add some timings for different steps timings:
```PHP
// at the very first line of controller to check
$GLOBALS['DBG_PERF_START'] = microtime(true);

// function to be included for any page
function ServerTiming($action_)
{
	static $counter = 0;
	$counter +=1;
	$counter_str = sprintf("%02d_%s", $counter, $action_);
	header("Server-Timing: $counter_str;dur=".((microtime(true)-$GLOBALS['DBG_PERF_START'])*1000), false);
	$GLOBALS['DBG_PERF_START'] = microtime(true);
}

// ... some code

ServerTiming("total"); // DBG
```

Timings are supposed to be in `ms`.
Multiple headers can be sent for different sections of the program, or even between programs/servers.

Ine the dev tools, each header entry will be alphabetically sorted, so I usually give names starting with numbers so I see the details in order of execution of the program, that's less confusing.

# Using with Postman
It's possible to show the custom server headers per request.

## 1. First create a new package
In the `package library`, add package name `server-timing`.
```JS
const template = `
    <table>
        <tr>
            <th>Key</th>
            <th>Timing (sec)</th>
        </tr>
        {{#each timings}}
            <tr>
                <td>{{key}}</td>
                <td style="text-align: right;">{{timing}}</td>
            </tr>
        {{/each}}
    </table>
`;

/**
 * @return {Object[]}
 * example: [{key: xxx, timing: yyy}, ...]
 */
function listTimings() {
    let headers = pm.response.headers.all();
    return headers.filter(header => {
        return /Server-Timing/.test(header.key);
    }).map(timing => {
        let matched = /^(.*);dur=(.*)/.exec(timing.value);
        // time in seconds
        let timeSec = matched[2]/1000;
        let timeFmt = /([0-9]+\.[0-9]{3})/.exec(timeSec);
        return {key: matched[1], timing: timeFmt[1]};
    });
}

module.exports = {
    visualize: function() {
        pm.visualizer.set(template, {
         timings: listTimings()
        });
    }
}
```

## 2. Setup a post-response script
Insert the following in `scripts>post-response` :
```JS
const myPackage = pm.require('server-timing')
myPackage.visualize();
```

![main script](rsc/Pasted image 20240618141213.png)

## 3. Watching response headers results
After the request is finished, the `Body>Visualize` section will show out script output.

![watching](rsc/Pasted image 20240618141226.png)
