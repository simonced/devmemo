<?php

class DBER {
	const GRAPHVIZ = "\"C:\\Program Files (x86)\\Graphviz2.38\\bin\\neato.exe\"";
	public $dbName = '';
	private $db; // Mysqli instance


	/**
	 * create an instance
	 *
	 * @param string $host_ DB host
	 * @param string $user_ DB user
	 * @param string $pwd_ DB password
	 * @param string $name_ DB name
	 */
	function __construct(string $host_, string $user_, string $pwd_, string $name_)
	{
		$this->dbName = $name_;
		// create connexion to DB
		$this->db = new Mysqli($host_, $user_, $pwd_, $name_);
		if(!$this->db) throw new Exception("Unable to connect to DB");
	}


	/**
	 * read tables from the DB
	 *
	 * @return array (list of tables in the DB)
	 */
	private function readTables()
	{
		$rs = $this->db->query("show tables;");
		if(!$rs) throw new Exception(__METHOD__ . ": failed to list tables.");

		$tables = [];
		foreach($rs->fetch_all() as $table)
		{
			$table_name = $table[0];
			$tables[$table_name] = $this->readTableFields($table_name);
		}
		return $tables;
	}


	/**
	 * read sql fields from the table
	 *
	 * @param string $table_
	 * @return array (list of columns)
	 */
	private function readTableFields(string $table_)
	{
		$rs = $this->db->query("show columns from $table_");
		if(!$rs) throw new Exception(__METHOD__ . ": failed to read table $table_ columns.");

		$cols = [];
		foreach($rs->fetch_all() as $columns)
		{
			$cols[] = $columns[0];
		}

		return $cols;
	}


	/**
	 * Make links between tables
	 *
	 * @param array $tables_
	 * @return array
	 */
	private function makeLinks(array $tables_)
	{
		$links = [];
		foreach($tables_ as $t => $_)
		{
			$links = array_merge($links, $this->searchLinksWith($t, $tables_));
		}

		return array_unique($links);
	}


	/**
	 * main function, matches a table name ($target_)
	 * against all other tables
	 *
	 * Current matching pattern:
	 *  `member_id` column linking to `member` table
	 *
	 * @param string $target_
	 * @param array $tables_
	 * @return array (list of links to target table as string for Graphviz)
	 */
	private function searchLinksWith(string $target_, array $tables_)
	{
		$links = [];

		// for each table
		foreach($tables_ as $t => $columns)
		{
			if($t == $target_) continue; // no link to ourself

			// for each column in each table
			foreach($columns as $column) {
				// we skip columns that do not have _id at the end
				if(!preg_match("/_id$/", $column)) continue;

				// strip _id at the end (if present)
				$table_name_from_col = preg_replace("/_id$/i", "", $column);
				// if match with target table name, add link
				if($table_name_from_col == $target_) $links[] = "$target_->$t;";
			}
		}

		return $links;
	}


	/**
	 * generate the graphical representation of the map
	 * output file in PNG
	 *
	 * @param string $outputFileName_
	 * @return string output result file name
	 */
	function generateMap(string $outputFileName_)
	{
		$tables = $this->readTables();
		// print_r($tables); // DBG
		$links = $this->makeLinks($tables);
		// print_r($links); // DBG
		$links_str = join(PHP_EOL, $links);

		$gv = <<<GV
digraph DBER {
node [shape=box];
fontsize=12;
overlap=false;
$links_str
}
GV;
		// print $gv; // DGB

		// file names
		$tempFile = "$outputFileName_.txt";
		$outputFile = "$outputFileName_.png";
		$path = __dir__;

		// process
		file_put_contents($tempFile, $gv);
		$command = self::GRAPHVIZ . " -Tpng $path\\$tempFile > $path\\$outputFile";
		// print_r($command);
		$success = system($command);

		return $outputFile;
	}
}
