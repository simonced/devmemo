<?php
/**
 * Simply for debugging memory usage.
 *
 * Sample:
 * MemoryStuff::before("doing stuff"); // will display and remember memory usage at that time
 * ... // do heavy stuff
 * MemoryStuff::after(); // will display the memory usage and the difference
 *
 * Also, it's possible to see what was the memory peak during execution:
 * MemoryStuff::peak();
 */

class MemoryStuff
{
    private static $call_count=0;
    private static $something;
    private static $mem1;
    private static $mem2;


    static function before($something_)
    {
        ++self::$call_count;
        self::$something = $something_;
        self::$mem1 = memory_get_usage(true);
        echo self::$call_count.". Before $something_: ".number_format(self::$mem1);
        echo "\n";
    }


    static function after()
    {
        self::$mem2 = memory_get_usage(true);
        echo "After ".self::$something.": ".number_format(self::$mem2);
        echo "\n";

        $diff = self::$mem2-self::$mem1;
        echo "Diff: ".number_format($diff)."\n";
    }


    static function peak()
    {
        $peak = memory_get_peak_usage(true);
        echo "Peak: ".number_format($peak)."\n";
    }
}