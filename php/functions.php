<?php
/**
 * http identification, like htaccess
 */
function http_login($login_, $mdp_) {
	if ($_SERVER['PHP_AUTH_USER']!="$login_" || $_SERVER['PHP_AUTH_PW']!="$mdp_") {
		header('WWW-Authenticate: Basic realm="Identification"');
		header('HTTP/1.0 401 Unauthorized');
		echo "You need to login to access the current website.";
		exit;
	}
}


/**
 * The goal is to mix those evenly, even when their length is different
 *
 * @param array $l1
 * @param array $l2
 * @return array
 */
function spreadLists($l1, $l2)
{
    $res = [];

    // length comparison
    $size1 = count($l1);
    $size2 = count($l2);
    // echo $size2; // DBG

    // swap them if l2 is longer, so it'll be the outter data
    if($size1 < $size2) return spreadLists($l2, $l1);

    $ratio = (int) $size1 / $size2;
    reset($l2);
    for($i=0; $i<$size1; ++$i)
    {
        $res[] = $l1[$i];
        if($i % $ratio == 0 && current($l2)) {
            $res[] = current($l2);
            next($l2);
        }
    }

    return $res;
}
