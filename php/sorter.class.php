<?php

/**
 * Sorting data helper class
 *   this can NOT deal with multiple sortings on the same page
 *   at the moment (because not needed yet)
 *
 * usage sample (let's call our page articles):
 *  in controller: 
 *     Sorter::setPage('articles'); // init
 *     
 *     Url like articles.php?sort=date&order=desc
 *     Sorter::params($_GET); // deal with get parameters `sort` and `order`
 *     
 *     query of articles:
 *     $sql = "select * from articles ".Sorter::getSortFull();
 *     // run query etc...
 *
 *  in template:
 *    <th><a href="?<?=Sorter::getUrl('id')?>">ID</a><?=Sorter::getIndicator('id')?></th>
 *
 */
class Sorter {

	private static $page = null;

	static function setPage($page_)
	{
		self::$page = $page_;
	}


	static function setSort($sort_)
	{
		$key = 'sort-'.self::$page;
		$_SESSION[$key] = $sort_;
	}


	static function setOrder($order_)
	{
		$key = 'order-'.self::$page;
		$_SESSION[$key] = $order_;
	}


	static function getSort()
	{
		$key = 'sort-'.self::$page;
		return $_SESSION[$key]?:'id';
	}


	static function getOrder($reverse_=false)
	{
		$key = 'order-'.self::$page;
		$order = $_SESSION[$key]?:'asc';

		if($reverse_) {
			$order = $order=='asc' ? 'desc' : 'asc';
		}

		return $order;
	}


	static function getSortFull()
	{
		return self::getSort().' '.self::getOrder();
	}


	static function params($get_)
	{
		if(isset($get_['sort'])) {
			self::setSort($get_['sort']);
		}

		if(isset($get_['order'])) {
			self::setOrder($get_['order']);
		}
	}


	// for templates


	static function getUrl($field_)
	{
		if($field_==self::getSort()) {
			$url_params = "sort=$field_&order=".self::getOrder(true);
		}
		else {
			$url_params = "sort=$field_&order=asc";
		}

		return $url_params;
	}


	static function getIndicator($field_)
	{
		if($field_==self::getSort()) {
			$indicator = self::getOrder()=='asc'?'▲':'▼';
		}
		else {
			$indicator = '';
		}

		return $indicator;
	}
}
