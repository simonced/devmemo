<?php
namespace logging;

class Logger
{
	private $path = '';
	private $file = '';

	/**
	 * Create the logger object
	 *
	 * @throws \Exception
	 * @param string $path_ where the logs should be saved (no trailing /)
	 */
	function __construct($path_)
	{
		$this->path = $path_;
		$this->checkLogPath();
		$file = $this->makeLogFileName();
		$this->file = $this->path.'/'.$file;
	}


	/**
	 * Check log path,
	 * create with subfolders if needed.
	 *
	 * @throws \Exception
	 * @return bool
	 */
	private function checkLogPath()
	{
		if(is_dir($this->path)===false) {
			// try to crate path
			$ok = mkdir($this->path, 0777, true);
			if($ok==false) {
				throw new \Exception("Could't create the log folder: {$this->path}");
			}
		}

		return true;
	}


	/**
	 * Create the log filename.
	 * (daily naming)
	 *
	 * @return string
	 */
	private function makeLogFileName()
	{
		return date("Y-m-d").'.txt';
	}


	/**
	 * Write to log file.
	 *
	 * @param string $content_ (no line break on last line)
	 * @return string the line added to log file (includes line-break)
	 */
	function logWrite($content_)
	{
		$content = date("Y-m-d H:i:s")."\t".$content_.PHP_EOL;
		file_put_contents($this->file, $content, FILE_APPEND);
		return $content;
	}


	/**
	 * List log files. (useful for admin)
	 *
	 * @return string[]
	 */
	function listLogs()
	{
		$files = []; // return
		$dd = opendir($this->path);

		while($entry = readdir($dd)) {
			if($entry=='.' || $entry=='..') continue;
			$files[] = $entry;
		}
		closedir($dd);

		return array_reverse($files);
	}


	/**
	 * Read log file.
	 *
	 * @param string $log_ (file name only)
	 * @return string (log file content)
	 */
	function readLog($log_)
	{
		return file_get_contents($this->path.'/'.$log_);
	}

}
