<?php


/**
 * Fixes links in content.
 * Helper function for checkandfixlinksIncontent()
 * Fixes href attributes in links provided.
 *
 * @param string $content_
 * @param array[array] $links_
 *  i.e: [0 => [
 * 			0 => "<a href='/privacy/'>Privacy</a>,
 * 			1 => '/privacy/']]
 * @return string
 * The matching links will be fixed like so:
 *   <a href="https://domain.com/whatever/privacy/">Privacy</a>
 *   where 'whatever' is the subfolder location.
 *   It has to be set in WP accordingly as well for matching to work.
 */
function checkAndFixLinks($content_, $links_)
{
	$content = $content_;
	// var_dump( $links_ ); // DBG

	foreach($links_ as $l) {
		// var_dump($l); // DBG
		$new_link = str_replace($l[1], home_url($l[1]), $l[0]);
		// var_dump($new_link); // DBG
		$content = str_replace($l[0], $new_link, $content);
	}

	return $content;
}

/**
 * Will analyse content before rendering
 * will look for links hard-coded and started with "/"
 * when such link is found, we apply home_url() to it.
 * This prevents the site to break when temporarily installed in a sub folder.
 *
 * @param string $content_
 * @return string
 */
function checkAndFixLinksInContent($content_)
{
	$found = preg_match_all("!<a.*?href=['\"](/.*?)['\"].*?</a>!", $content_, $matches, PREG_SET_ORDER);
	if(false !== $found && count($matches) > 0) {
		$content = checkAndFixLinks($content_, $matches);
	}
	else {
		$content = $content_;
	}

	return $content;
}
add_filter('the_content', 'checkAndFixLinksInContent');
