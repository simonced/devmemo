<?php

/**
 * Create a pagination in HTML
 *
 * @param string $url_ Basic ULR for each page, %s marks the page number。
 * @param int $total_ total number of pages
 * @param int $page_ current page
 * @return string (HTML)
 */
function theme_paging($url_, $total_, $page_=1)
{
	$html = "<ul class='paging'>\n";
	$dot_padding = 2;
	// [1] 2 3 ... 41 42 43
	// 1 [2] 3 4  ... 41 42 43
	// 1 2 3 4 5 [6] 7 8 ... 41 42 43
	// 1 2 3 ... 5 6 [7] 8 9 ... 41 42 43
	// 1 2 3 ... 39 40 [41] 42 43

	if($page_>1) {
		$url = sprintf($url_, 1);
		$html .= "<li class='page_first'><a href='$url'>&laquo;</a></li>\n";
		$url = sprintf($url_, $page_-1);
		$html .= "<li class='page_prev'><a href='$url'>&lsaquo;</a></li>\n";
	}

	$chunk1_s = 1;
	$chunk1_e = 1+$dot_padding;
	$chunk2_s = $page_-$dot_padding;
	$chunk2_e = $page_+$dot_padding;
	$chunk3_s = $total_-$dot_padding;
	$chunk3_e = $total_;
	$dots_added = false;
	for($i=1; $i<=$total_; ++$i) {
		$url = sprintf($url_, $i);
		if($i==$page_) $class="class='page_current'";
		else $class = "";

		if($chunk1_s<=$i && $i<=$chunk1_e) {
			$html .= "<li $class><a href='$url'>$i</a></li>\n";
			$dots_added = false;
		}
		elseif($chunk2_s<=$i && $i<=$chunk2_e) {
			$html .= "<li $class><a href='$url'>$i</a></li>\n";
			$dots_added = false;
		}
		elseif($chunk3_s<=$i && $i<=$chunk3_e) {
			$html .= "<li $class><a href='$url'>$i</a></li>\n";
			$dots_added = false;
		}
		else if(!$dots_added) {
			$html .= "<li>...</li>\n";
			$dots_added = true;
		}
	}

	if($page_<$total_) {
		$url = sprintf($url_, $page_+1);
		$html .= "<li class='page_next'><a href='$url'>&rsaquo;</a></li>\n";
		$url = sprintf($url_, $total_);
		$html .= "<li class='page_last'><a href='$url'>&raquo;</a></li>\n";
	}

	$html .= "</ul>\n";

	return $html;
}