
# Simple redirections
## http -> https without the www
In the sample below, replace `<MYDOMAIN>` with what domain you need to redirect to.

```apache
# redirect if NOT https, OR www present
RewriteCond %{HTTPS}        =off   [OR]
RewriteCond %{HTTP_HOST}    ^www   [NC]
RewriteRule ^(.*)$          https://<MYDOMAIN>%{REQUEST_URI} [R=301,L]
```
