
# Introduction

This is one common setup I use often.  
I allows to only have one "modal" per page. I'll improve on that when I need multiple modals on the same page.

The principle is simple, having a basic "container" markup and the layout is all done in CSS.  
The user action is dealt with JS.

# Markup sample

```html
<div id="my-modal" class="modal-filter">
	<div class="modal-container">
		<a class="btn btn-close" href="#close">Close</a>
		<div class="modal-title">TITLE</div>
		<div class="modal-scroller">
			MY CONTENT!
		</div>
	</div>
</div>
```

# Css

```css
/* modal! */
body.modal-open {
    overflow: hidden;
}

body.modal-open .modal-filter {
	opacity: 1;
	z-index: 110;
}

.modal-filter {
	opacity: 0;
	background: rgba(0, 0, 0, 0.9);
	position: fixed;
	z-index: -1;
	top: 0;
	left: 0;
	bottom: 0;
	right: 0;
	transition: opacity 0.4s;
}

.modal-container {
	margin: 7vh 10vw;
	position: relative;
}

.modal-container .btn-close {
	position: absolute;
	top: 0;
	right: 0;
	background: white;
	color: black;
}

.modal-title {
	font-size: 25px;
	color: white;
	text-align: center;
}

.modal-scroller {
	overflow-y: auto;
	max-height: 80vh;
}
```

# JS
Using jQuery (for example), a modal can be opened like so:

```js
// opening the modal (code to put in the handler of a user action like click)
$("body").addClass("modal-open");

// closing the modal
$('.modal-container .btn-close').click(function(e_) {
	e_.preventDefault();
	$("body").removeClass("modal-open");
});
```

Yes, we add the `modal-open` class to the `body` tag.  
Why? Because it allows to "lock" the scrolling on the body if we have to.
