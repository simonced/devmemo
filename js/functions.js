/**
 * returns the screen/window/viewport size
 * tested on:
 * - IE 11
 * - Edge
 * - PC Chrome
 * - Android 7 Chrome
 * - iPad (iOS 9)
 * @return array {width, height}
 */
function viewport() {
	var e = window, a = 'inner';
	if (!('innerWidth' in window )) {
		a = 'client';
		e = document.documentElement || document.body;
	}
	return { width : e[ a+'Width' ] , height : e[ a+'Height' ] };
}


// we force viewport size so content fits on any mobile screen below a certain resolution >>>
function setViewport() {
    var winSizeTarget = 640;
    var winWidth = $(window).width();

    if(winWidth<winSizeTarget) {
        viewport = document.querySelector("meta[name=viewport]");
        viewport.setAttribute('content', 'width='+winSizeTarget+', user-scalable=no');
    }
}
// <<<


/**
 * animates scrolling to anchor tags
 * (see sample usage in README)
 *
 * depends on:
 * - jQuery
 */
var AnchorScroller = {
    scrollOffset: 0, // if needed, use that offset so the view is scrolled accordingly

    scrollTo: function(hash_) {
        try {
            var trgt_id = hash_.replace('#', '');
            var trgt = $('a[name='+trgt_id+']')[0];
            //console.log(trgt);
            var top_pos = $(trgt).offset().top - this.scrollOffset;
            $('body, html').animate({scrollTop: top_pos});
        }
        catch(e) {
        }
    },

    check: function() {
        // check after loading (called from document.ready event)
        if( window.location.hash ) {
            AnchorScroller.scrollTo( window.location.hash );
        }
    }
}

// =====================================================================

/**
 * Makes tables scrollable when screen space is limited.
 * (see sample usage in README)
 *
 * depends on:
 * - jQuery
 */
var MobileScroller = {
	addScrollMask : function() {
		// this is the table container

		// mask creation
		var mask = $('<div class="mobile-scroll-mask"></div>');

		// adding mask to container
		$(this).prepend(mask);

		// action on the mask
		mask.click(function() {
			$(this).fadeOut();
		});
	}
};
