Some things work in some browsers but not all.  
Here is a list of problems we encountered during our developments.

# 🧭 Usages
Help on the helper scripts we hae here.

## FixedMenu
This makes a menu static on screen after scrolling a certain distance.  
The distance is specified with a container, the height is calculated automatically.

**File:** _fixed-menu.js_

```javascript
// call in document ready event for example
FixedMenu.init("#nav-menu", "header");
```

Sample HTML:

```html
<header>
lorem ipsum
</header>
<nav id="nav-main">
...
</nav>
```

## TopArrow
It helps to display an arrow (fixed position) in the screen to "jump" back at the top of the page.  
**File:** _top-arrow.js_

```javascript
// simply call: (best in ready event)
TopArrow.init();
```

Sample CSS to ge started:

```css
#TopArrowScrollBtn {
	position: fixed;
	right: 10px;
	bottom: 10px;
	padding: 10px;
	background-color: rgba(0, 0, 0, 0.8);
	border-radius: 5px;
	color: white;
	font-weight: bold;
	text-decoration: none;
}
```

## Collapsable
Simple utility to collapse/expand blocks.  
**File:** _collapsable.js_

Buttons need to have `data-toggle=collapse` attribute.  
Targets need `class=collapse` to be automatically closed  
as well as an ID with the same `href` as in the buttons.

Sample:

```html
<a href='#mypanel' data-toggle='collapse'>View Details</a>
<p id='mypanel' class='collapse'>
  Lorem, ipsum dolor sit amet consectetur adipisicing elit.
  Minus nihil harum quaerat, tempore animi beatae modi,
  similique quasi nostrum alias consectetur excepturi fugit labore quas
  dignissimos provident odit maiores! Maiores!
</p>

<script>
Collapsable.init(true); // that's all
</script>
```

## AnchorScroll
Sample usage: (in a document.ready event for example)

```javascript
// scroll after page loaded
AnchorScroller.check();

// scroll on specific links actions (clicks)
$('header a').click(function(e_) {
	e_.preventDefault();
	AnchorScroller.scrollTo( $(this).attr('href') );
});
```

HTML sample:

```html
<header>
	<nav>
		<a href="#content1">Content1</a>
		<a href="#content2">Content2</a>
	</nav>
</header>
```

## Mobile Scroll
Some data can only be displayed as tables.  
In those cases, a simple way to display them on mobile is to make them scroll.

The setup is not complex, here is how to use the `MobileScroll` javascript:

```javascript
// good to set up in a ready() event for example

// container of our table
$(".mobile-scroll").each(MobileScroller.addScrollMask);
```

Html sample:

```html
<div class="mobile-scroll">
	<table>
		<!-- many lines of data!!! -->
	</table>
</div>
```

And that's it for the data!

Basic CSS to get started:

```css
.mobile-scroll { /* table container */
	position: relative;
	overflow-x: auto;   /* you could make the content scrollable in X and Y or only Y if you want */
}
.mobile-scroll-mask { /* will be added dynamically by the Javascript */
	position: absolute;
	display: none;
	top: 0;
	right: 0;
	bottom: 0;
	left: 0;

	/* if you use a background picture  (no need of the ::before definition below in that case) */
	/* background: rgba(0, 0, 0, 0.4) url(path to background icon...) top center no-repeat; */

	/* if you use text in ::before */
	background: rgba(0, 0, 0, 0.4);
}
.mobile-scroll-mask::before { /* no need if you use a background picture */
	content: "TAP to scroll";
	display: block;
	text-align: center;
	color: white;
	margin-top: 3em;
}
@media (max-width: 700px) {  /* set the viewport dimensions when you want to scroll mask to be displayed */
	.mobile-scroll-content {
		min-width: 750px;   /* adapt here for the minimum size of your data */
	}
	.mobile-scroll-mask {
		display: block;
	}
}
```

## Table Of Content Generator

It might be useful (for documentation) to generate a table of content from the content.  
It's good for content that changes often, so the TOC is always up to date.  
**File:** *toc-generator.js*

The different elements are indented with some CSS like so:

```css
.level2 {
	margin-left: 2em;
}
```

The generated TOC needs a place to be inserted, like so:

```html
<nav id="toc"></nav>
```

And some content could look like so:

```html
<!-- section tags are not mendatory -->
<section>
	<h3>My content</h3>
	<p>blah blah</p>

	<section>
		<h4>My second level content</h4>
		<p>aaa</p>
	</section>
</section>
```

Generating the TOC like so:

```js
$('#toc').append( TOCgenerator.init() );
```

This is a very simple script and no configuration is available yet.  
Please refeer to the source code (`<Customize here>` comment) to where customize the content headers and the indentation.


## Links History
This little lib simply remembers in `sessionStorage` what links a user clicks.  
These links get a `visited` class applied to them, even on page reload/transition.  
**File:** *links-history.js*

Sample usage:

```js
LinksHistory.init("a.mylinks");

```

## Choice Selection

This is how to use the `choice_selection.js`.  
This shows a dropdown list of possible entries under a text field.

HTML sample (using Bootstrap CSS):

```HTML
<!-- year is single matching -->
<input type="number" name="year" id="year" autocomplete="off" class="form-control" value="">
<div class="p-absolute">
	<div id="years-list" class="list-group">
		<a href="#" class="list-group-item list-group-item-action" data-matching="2025">2025</a>
		<a href="#" class="list-group-item list-group-item-action" data-matching="2026">2026</a>
		<a href="#" class="list-group-item list-group-item-action" data-matching="2027">2027</a>
	</div>
</div>

<!-- year is multi matching (data-matching has multiple values, separator is arbitrary) -->
<input type="text" name="school" id="school" autocomplete="off" class="form-control" value="">
<div class="p-absolute">
	<div id="schools-list" class="list-group">
		<a href="#" class="list-group-item list-group-item-action d-none" data-year="2025" data-matching="aaa,bbb,ccc">aaa</a>
		<a href="#" class="list-group-item list-group-item-action d-none" data-year="2025" data-matching="aaa2, bbb2">aaa2</a>
	</div>
</div>
```

JS to go with it:

```JS
let yearChoice = new ChoiceSelector("#year", "#years-list");
let schoolChoice = new ChoiceSelector("#school", "#schools-list");
schoolChoice.addLink(yearChoice.input, "data-year");
```

This makes 2 dropdown lists for 2 input fields: `year` and `school`.  
There is a feature to "link" the choices available from one field to the other.  
In the sample above, the `school` choices depend on the selection of `year`.

When linking, we need a second info with the value to match.  
The sample shows `data-year` which is an attribute in the list of schools we have.


# 🪄 Tips and Tricks

Nothing really fancy, but we forget sometimes. (more than we agree to admin...)

## Use jQuery `$` in our Wordpress Template:

```javascript
(function ($) {
	// you can use $ here now, it's the parameter of the anonymous function
})(jQuery);
```

## Browser detection

Simple snippets I use all the time.

```javascript
// IE11 detection (can be adapted to other browsers)
var isIE11 = /trident/i.test(navigator.userAgent);
if(isIE11) {
	document.getElementsByTagName("body")[0].classList.add("IE11");
}
```

And then, we can use targeted CSS, it's better than hacks...

```css
.IE11 div.page {
	display: block;
	min-height: auto;
}
```

## Menu on MouseOver (PC and iOS)

The hurdle is real when it comes to make submenus display on mouse hover event on PC AND to support touch devices.

Structure sample:
```html
<header>
  <ul>
	<li><a href="#" class="has-submenu">Entry 1</a>
	  <div class="submenu">Entry 2</div>
	</li>
	<li><a href="#" class="has-submenu">Submenu2</a>
	  <div class="submenu">Submenu content</div>
	</li>
	<li><a href="#">No submenu Item</a></li>
  </ul>
</header>
```

Here is the little script I tend to use for simple 1 sub-level menus:
```javascript
// iOS
$("header .has-submenu").on("touchend", function(e_) {
	e_.stopPropagation();
	$("header li.opened").not($(this).parent()).removeClass("opened");
	$(this).parent().toggleClass("opened");
	return false;
});
$("body").on("touchend", function(e_) {
	$("header li.opened").removeClass("opened");
});

// PC
$("header .has-submenu").mouseenter(function(e_) {
	$("header li.opened").removeClass("opened");
	$(this).parent().addClass("opened");
});
$("header .header-submenu").mouseleave(function(e_) {
	$(this).parent().removeClass("opened");
});
```

Tested on Chrome 75, Edge, IE 11, iOS 12 Safari.

# 🪝 Fetch API
Here a re some samples I use frequently.  
Very easy to use, no need of JQuery once we understand Promises.

## Basic usage

```JS
let options = {};
fetch("my URL", options)
.fetch(response => {
	// do stuff...
})
.catch(error => {
	// something went wrong...
	// This part has to be after the different "then" sections.
});
```

Because fetch API returns a promise, the `then` sections are resolvers.  
They can be "chained" to parse the results and run different functions client side.  
The `options` object can contain many useful things, described below.

## Parse JSON answer
Here again, very simple.  
For this example, no options for simplicity sake.

```JS
fetch("my URL")
.then(response => response.json())
.then(data => {
	// do stuff with `data` that is parsed from response as JSON data.
	// here, `data` is an object we can use as we see fit...
})
.catch(error => {
	// deal with error
	// if `response.json()` fails, it`ll be catched here.
});
```

## Send Post data like a normal form
This is useful because JSON payload are not straightforward to handle with PHP.

```JS
let body = new URLSearchParams({
	"my variable 1": content1,
	// ...
});

let options = {
	method: "POST",
	headers: {
		"Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
	},
	body: body.toString(),
};
```

`body` variable can contain anything.  
That's it!

## Timeout request
To be sure a request doesn't wait forever, we can cancel it after some time.  
For so, we use an `AbortSignal`.

```JS
let options = {
	signal: AbortSignal.timeout(5000); // wait for 5 seconds
};
```

If the request takes to long, an error will be thrown.  
This has to be caught in a `catch` function.

# Working with dates
Simple snippet to format a date in Japanese with 2 digits:

```JS
/**
 * Make a datetime string in Japanese format
 * @param {Date} date datetime in JS Date object
 * @return {string} datetime in "YYYY年MM月DD日" format
 */
function makeDateJP(date) {
	let month = Intl.NumberFormat('ja-JP', {minimumIntegerDigits: 2}).format(date.getMonth()+1);
	let day = Intl.NumberFormat('ja-JP', {minimumIntegerDigits: 2}).format(date.getDate());
	return `${date.getFullYear()}年${month}月${day}日`;
}
```

# Misc.

**2024-07-24**

Add `choice_selection.js` script that makes autocompletion for input fields.

**2018-05-18**

Sometimes, IE will use Quirk mode in a different version and JS will return errors etc...  
To prevent that, always set `<meta http-equiv="x-ua-compatible" content="IE=edge">` in the `header` tag of the HTML pages!

**2017-02-06**

`document.activeElement` is not refering to the correct element in *Mac Safari*.  
replacement: `window.event.target`.  
This happened with an event fired by Bootstrap when closing a modal window.
