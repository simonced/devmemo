/**
 * This lib generates a clickable table of content from content.
 * Default usage is to search for h3 and h4 tags.
 * The h3 tags with "ignore" class are not inserted in the TOC.
 * Those can be customized, see <Customize here> in source comments.
 * The level of identation is a simple class with a margin-left.
 *
 * depends on:
 * - jQuery
 *
 * Sample usage: (the target is a <nav></nav> empty element with id "summary")
 *  $('#toc').append( TOCgenerator.init() );
 */
var TOCgenerator = {
    init: function() {
        var menuList = $("<ul></ul>");
        var link, level, linkID, entry, anchor;
        linkID = 0;

        // what entries are going into the table of Content?
        // <Customize here>
        $('h3:not(.ignore), h4').each(function() {
            // console.log( $(this) );

            ++linkID;

            // simple indentation
            // <Customize here>
            switch($(this)[0].tagName) {
                case 'H3':
                default:
                    level = "level1";
                break;

                case 'H4':
                    level = "level2";
                break;
            }

            link = $('<a></a>');
            link.attr("href", "#link"+linkID);
            link.html($(this).html());

            // create target anchor tag
            anchor = $("<a></a>");
            anchor.attr("name", "link"+linkID);
            $(this).before(anchor);

            // add entry to menu
            entry = $("<li></li>");
            entry.addClass(level);
            entry.append(link);
            // add entry to menu list
            menuList.append(entry);
        });

        return menuList;
    },
};