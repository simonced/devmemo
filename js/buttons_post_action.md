
# Change page from button click with post request
Here is a simple way to make a JS action for a button (or a link) work as a form.  
This allows to send data to the next page without making a real form, and without adding data into the URL.

Make the following a function:
```JS
// Create new form and add it into the page
var f = document.createElement('form');
window.top.document.body.appendChild(f);
f.method = "POST";
f.action = '/my_controller.php'; // TODO customize here

// TODO create necessary inputs to send with the post form
var input = document.createElement("input");
input.type = 'hidden';
input.name = 'pgm';
input.value = 'my data';
f.appendChild(input);

// let's go!
f.submit();
```

Then, simply call the function within a `onclick` envent, that's it!
