
/**
 * Collapsable utility
 * depends on:
 *  - jQuery
 * buttons need to have 'data-toggle=collapse' attribute
 * targets need 'class=collapse' to be automatically closed
 * as well as an ID with the same 'href' as in the buttons.
 *
 * Sample:
 * <a href='#mypanel' data-toggle='collapse'>View Details</a>
 * <p id='mypanel' class='collapse'>
 *   Lorem, ipsum dolor sit amet consectetur adipisicing elit.
 *   Minus nihil harum quaerat, tempore animi beatae modi,
 *   similique quasi nostrum alias consectetur excepturi fugit labore quas
 *   dignissimos provident odit maiores! Maiores!
 * </p>
 *
 * <script>
 * Collapsable.init(true); // that's all
 * </script>
 */
var Collapsable = {

	init: function(hideTargets_) {
		// auto close targets
		if(hideTargets_ === true) {
			$('.collapse').hide();
		}

		// buttons actions
		$('[data-toggle=collapse]').click(function(e_) {
			e_.preventDefault();
			var target = $(this).attr('href');
			$(target).slideToggle('fast');
		});
	}
};
