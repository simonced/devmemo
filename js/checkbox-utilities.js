/**
 * This lib allows to check checkboxes as a range with shift
 * Tested on Chrome 74, IE 11 and Edge
 *
 * depends on:
 * - jQuery
 *
 * Sample usage:
 *  CheckUtility.checks = $("#jobs-list input[type=checkbox]");
 *  CheckUtility.checks.mouseup(CheckUtility.selection);
 */
var CheckUtility = {

    checks: null,
    lastActivated: null,

    // selection of all entries
    selectAll: function(e_) {
        e_.preventDefault();
        CheckUtility.checks.prop("checked", true);
    },

    // deselection of all entries
    selectNone: function(e_) {
        e_.preventDefault();
        CheckUtility.checks.prop("checked", false);
    },

    // JQuery event
    // change status of a check box
    selection: function(e_) {
        // console.log( e_.shiftKey );

        // a < val < b inclusive
        const isBetween = function(val, a, b) {
            return (a < b) ? (a <= val && val <= b) : (b <= val && val <= a);
        };

        // shift used?
        if(CheckUtility.lastActivated && e_.shiftKey) {
            // e_.preventDefault();

            // last clicked checkbox
            var currentActivated = CheckUtility.checks.index(this);
            var stat = $(CheckUtility.checks[CheckUtility.lastActivated]).prop("checked");

            CheckUtility.checks.each(function(index) {
                if(isBetween(index, CheckUtility.lastActivated, currentActivated)) {
                    if(index==currentActivated) return; // not change otherwise it's flickering in opposite state
                    $(this).prop("checked", stat);
                }
            });
        }

        // memo last clicked checkbox
        CheckUtility.lastActivated = CheckUtility.checks.index(this);
    },
};
