// helper function to remove duplicates in arrays
Array.prototype.unique = function() {
    return this.filter(function (value, index, self) {
        return self.indexOf(value) === index;
    });
}

/**
 * memorizes user click on links in session storage and apply
 * a specific CSS class (even at page load)
 *
 * Sample Usage:
 *   LinksHistory.init("a.mylinks");
 *
 * When user click those links, "visited" CSS class is added.
 * Style that as you see fit.
 *
 * @type {Object}
 */
var LinksHistory = {

    // visited links list
    links: [],

    init: function(selector_) {
        // restore visited links
        var links_json = window.sessionStorage.getItem('links-history');
        if(links_json != null) {
            this.links = JSON.parse(links_json);
        }

        // set click event on all links of a certain type
        var page_links = document.querySelectorAll(selector_);
        page_links.forEach( (link) => {
            // console.log( link );
            link.addEventListener("click", (event_) => { this.addToHistory(event_); }, false);
            // already visited?
            if(this.links.includes(link.href)) {
                link.classList.add("visited");
            }
        } );
    },


    /**
     * user link click adding to history handler
     * (comming from click callback)
     * scope: LinksHistory class
     * @param {Event} e_ the clicked link event
     */
    addToHistory: function(e_) {
        // console.log( e_.target ); // DBG
        // console.log( this ); // DBG
        e_.target.classList.add("visited");
        // add to links list
        this.links.push(e_.target.href);
        window.sessionStorage.setItem('links-history', JSON.stringify( this.links.unique() ));
    },
};