class ChoiceSelector
{
	input;
	list;
	links = [];

	/**
	 * @param {string} input_ 入力フィールドのQUERYセレクター
	 * @param {string} list_ 選択リストのQUERYセレクター
	 */
	constructor(input_, list_)
	{
		this.input = document.querySelector(input_);
		this.list = document.querySelector(list_);

		// input event
		this.input.addEventListener("focus", x => {
			this.updateList(x.target.value);
		});
		this.input.addEventListener("keyup", x => {
			this.updateList(x.target.value);
		});
		this.input.addEventListener("change", x => {
			this.updateList(x.target.value);
		});

		// click event on list elements
		this.list.addEventListener("click", y => {
			if(y.target.hasAttribute("data-matching")==false) return;
			y.preventDefault();
			this.select(y.target);
		});

		// blur event
		document.body.addEventListener("click", x => {
			if(x.target!=this.input) this.list.classList.add("d-none");
		});
	}


	/**
	 * input_の値と選択リストのattr_の値のマチングを比較する為、
	 * このペアを登録する（複数可能ですが、未確認）
	 *
	 * @param {Element} input_ 別入力フィールドの値を確認する為
	 * @param {string} attr_ 選択リストにあるHTMLattributeの名前
	 */
	addLink(input_, attr_)
	{
		this.links.push({input: input_, attribute: attr_});
		input_.addEventListener("change", x => {
			this.input.value = "";
		});
	}


	/**
	 * 入力された値から、選択リストを表示し、中身を絞り込む
	 *
	 * @param {string} input_ 入力された値
	 */
	updateList(input_)
	{
		// if(input_=="") this.list.classList.add("d-none");
		// else this.list.classList.remove("d-none");
		this.list.classList.remove("d-none");

		// filter content
		let choices = this.list.querySelectorAll("[data-matching]");
		choices.forEach(x => {
			let matching = x.getAttribute("data-matching");
			if(matching.includes(input_)) {
				x.classList.remove("d-none");
			} else {
				x.classList.add("d-none");
			}
			// match links
			if(this.links.length==0) return;
			this.links.forEach(l => {
				if(l.input.value != x.getAttribute(l.attribute)) x.classList.add("d-none");
			});
		});
	}


	/**
	 * 選択リストのアイテムをクリックしたときのハンドラー
	 *
	 * @param {Element} choice_ 選択リストの中からクリックされたアイテム自体
	 */
	select(choice_)
	{
		let content = choice_.innerText;
		this.input.value = content;
	}
}
